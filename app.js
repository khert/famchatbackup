var express = require('express');
var app = require('express')();
var http = require('http').Server(app);
var path = require('path'); 
var mongoose = require("mongoose");
mongoose.Promise = global.Promise;
var bodyParser = require('body-parser');
var Session = require('./models/sessionSchema');
var User = require('./models/userSchema');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var morgan = require('morgan');
var myRoutes = require('./routes/router');
var multer = require('multer');
var io = require('socket.io')(http);
var router = express.Router();




var SSE = require('express-sse');
var sse = new SSE(["array", "containing", "initial", "content", "(optional)"]);


mongoose.connect("mongodb://localhost/testdb");
var db = mongoose.connection;


db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (err) {
	if(err){

	}else{
		console.log('* Connected to Database ...');
	}
});

var storage = multer.diskStorage({
		destination : './uploads',
		filename : function(req,file,next){
			 next(null,file.fieldname + '-' +Date.now()+path.extname(file.originalname));
		}
});
var upload = multer({
	storage: storage
}).single('photo');



app.use(session({
  secret: 'work hard',
  resave: true,
  saveUninitialized: false,
  store: new MongoStore({
    mongooseConnection: db
  })
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine','ejs');
app.use('/bootstrap',express.static(path.join(__dirname,'bootstrap')));
app.use('/bootstrapV3.3.7',express.static(path.join(__dirname,'bootstrapV3.3.7')));
app.use('/views',express.static(path.join(__dirname,'views')));
app.use('/uploads',express.static(path.join(__dirname,'uploads')));
//app.use(morgan('dev'));
app.use(function(req, res, next) {
    req.io = io;
    next();
});


app.use(myRoutes.route);
app.use(router);
app.use(myRoutes.multer);

var sse = myRoutes.sse;

// MY ONLINE DATABASE **********************************************
//mongoose.connect("mongodb://khert:khert@ds121088.mlab.com:21088/mydb");
//*************************************************************

var clients = {};


router.get('/testtest',(req,res)=>{
	res.send('TESTTEST');
});



router.get('/uploadTwo',(req,res)=>{
	User.findById(req.session.userId,function(err,user){
		if(err){
			console.log('ERROR SAVING IMAGES');
		}else{
			User.findByIdAndUpdate(req.session.userId,{$set:{ "avatar":"bootstrapV3.3.7/pictures/"+user.gender+".png"}},{upsert:true},function(err,list){
				if(err){
					console.log('ERROR SAVING IMAGES');
				}else{
					 res.redirect('./home');
				}
			});
					
		}

	});
	
});





router.get('/sendToClient',function(req,res){
	

		var query = User.findOne({firstname:req.query.sendTo});
		query.exec(function(err,user){
			User.findById(req.session.userId)
	        .exec(function(err2,user2){
	        	if(err2){
	        		res.send('ERROR');
	        		console.log('ERROR')
	        	}else{

	        		console.log('SUCCESS SEND')
		        	

		        	io.sockets.in(user._id).emit('new_msg', {message: req.query.message, name:user2.firstname, lname:user2.lastname});
				
				    res.send({status:'success sending message..'});
				}

	        });
			
		});
});






io.on('connection', function(socket){

	socket.on('join', function (data) {

		console.log(' ******************  '+data.name + ' connected *************************');
		socket.username = data.name;
	    socket.join(data.name); // We are using room of socket io
	});

	socket.on('disconnect',function(client){
		console.log(' ******************  '+socket.username + ' disconnected *************************')
	});

});











http.listen(4000,function(){
	console.log('* Server Running ... ');
	console.log('* Server is on http://localhost:4000 ...');
});

