var mongoose = require('mongoose');
var Schema = mongoose.Schema;
mongoose.connect('mongodb://localhost/gridFS');
var conn = mongoose.connection;
var path = require('path');

var Grid = require('gridfs-stream');
var fs = require('fs');

//where to find the video in the filesystem that we will store in the DB
var videoPath = path.join(__dirname,'../readFrom/dfd.png');

//connect GridFS and mongo
Grid.mongo = mongoose.mongo;

conn.once('open',function () {
	console.log('- Connection open -');
	var gfs = Grid(conn.db);

	//when connection is open, create write stream with
	// the name to store file as in the DB
	var writestream = gfs.createWriteStream({
		//will be stored in Mongo as ''
		filename:'storeFile.png'
	});
	//create a read-stream from where the video currently is (videoPath)
	// and pipe it into the database (through write-stream)
	fs.createReadStream(videoPath).pipe(writestream);

	writestream.on('close',function(file){
		//do something with 'file'
		//console logging that is was written successfully
		console.log(file.filename + ' Written To DB');
	});
})