var mongoose = require('mongoose');
var Schema = mongoose.Schema;
mongoose.connect('mongodb://localhost/gridFS');
var conn = mongoose.connection;

var fs = require('fs');
var path = require('path');

var Grid = require('gridfs-stream');


Grid.mongo = mongoose.mongo;

conn.once('open',function () {
	console.log('- Connection open -');
	var gfs = Grid(conn.db);

	//write content from DB to file system with
	//the given name (in this case ,200lablaabll.mp4)
	var fs_write_stream = fs.createWriteStream(path.join(__dirname,'../writeTo/storeFile.png'));

	//create read=stream from mongodb
	//in this case , finding the correct file by 'filename'
	// but could also find by ID or other properties
	var readStream = gfs.createReadStream({
		filename:'storeFile.png'
	});
	//pipe the readstream in to the write stream
	readStream.pipe(fs_write_stream);

	fs_write_stream.on('close',function(){
		console.log('File has been written fully!');
	});

})