var express = require('express');
var app = require('express')();
var http = require('http').Server(app);
// var io = require('socket.io')(http);

var path = require('path'); 
var mongoose = require("mongoose");
mongoose.Promise = global.Promise;
var bodyParser = require('body-parser');
var User = require('./routes/userSchema');
var Session = require('./routes/sessionSchema');
// var pic = require('./routes/picSchema');




// var cookieParser = require('cookie-parser');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var morgan = require('morgan');


var hema = mongoose.Schema;

var picSchema = new hema({
	picData:String,
	
});

var Pic = mongoose.model('Pic',picSchema);

mongoose.connect("mongodb://localhost/testdb");
var db = mongoose.connection;

//handle mongo error
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (err) {
	if(err){

	}else{
		console.log('CONNECTED!!');
	}
  
});


app.use(session({
  secret: 'work hard',
  resave: true,
  saveUninitialized: false,
  store: new MongoStore({
    mongooseConnection: db
  })
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine','ejs');
app.use('/bootstrap',express.static(path.join(__dirname,'bootstrap')));
app.use('/views',express.static(path.join(__dirname,'views')));
app.use(morgan('dev'));


// OTHER DATABASE
//mongoose.connect("mongodb://localhost/famchat");
//mongoose.connect("mongodb://khert:khert@ds121088.mlab.com:21088/mydb");



app.get('/',function(req,res){
	// res.sendFile(__dirname+'/profile.html');
	// console.log(req.session);
	// console.log('=====================');
	// console.log(req.cookies);
	 res.render('index');
});

// WHEN THE USER ATTEMPTS TO LOG IN
// TESTED
app.post('/login',function(req,res){

	var query = User.findOne({username:req.body.username,passwordConf:req.body.password});
	//query.select('username password avatar');
	query.exec(function(err,user){
		if(err){
			res.send('ERROR !!');
		}else{
			if(user === null){
				res.send('NOT LOGIN');
			}else{
				req.session.userId = user._id;
				res.redirect('/home');
			}
		}
	});
});

// WHEN THE USER REGISTERS IN THE WEB
//TESTED
app.post('/register',function(req,res){
	var newUser = new User();
	newUser.username = req.body.username;
	newUser.email = req.body.email;
	newUser.password = newUser.generateHash(req.body.password);
	newUser.passwordConf = req.body.password;
	newUser.save(function(err,user){
		if(err){
			res.send('ERROR!!');
		}else{
			res.render('setProfile',{name:req.body.username});
		}
	});
});

//UPDATE 
//NOT YET TESTED
app.put('/update:id',function(){
	User.findOneAndUpdate({
		_id:req.params.id
	},{$set:{username:req.body.username}}
	,{upsert:true}
	,function(err,newUser){
		if(err){
			console.log('errrorr');
		}else{
			res.status('204');
		}
	});
});
app.get('/home',function(req,res){

	User.findById(req.session.userId)
	.exec(function(err,user){
		if(err){
			res.send('ERROR!!')
		}else{
			if(!user){
				res.send('ERROR USER NOT AUTHENTICATED!!');
			}else{
				res.render('home',{avatar:user.avatar,username:user.username});
			}		
		}
	});
});

app.get('/logout',function(req,res){

	    // req.session = null;
	    // res.redirect('/');
	    // var query = Session.remove({userId:req.session.userId});

	    // query.exec(function(err){
	    // 	if(err){
	    // 		res.send('ERROR');
	    // 	}else{
	    // 		res.send('SUCCESS');
	    // 	}
	    // })
	   	// res.json(req.session.userId)

	    // res.json(req.session);
	    // req.session.destroy();
	    // res.redirect('/');
	    if(req.session){
			req.session.destroy(function(err){
				if(err){
					res.send('ERROR'+' ');
				}else{
					console.log('OLRIGHT ============================');
					res.redirect('/');
				}
			});
		}
});


// io.on('connection', function(socket){
// 	socket.on('getData',function(data){
// 		console.log(data.name + " this is from getData function");
// 		var query = { username: data.name };
// 		User.findOneAndUpdate(query,{$set:{avatar:data.message}},function (err,newUser) {
// 			if(err){
// 				console.log('ERORR in socket getData!!');
// 			}else{
// 				console.log(newUser);
// 			}
// 		});
// 	});
// });

app.get('/profile/:id',function(req,res){
	res.send('THe id is '+ req.params.id);
});

http.listen(4000,function(){
	console.log('* Server Running ... ');
	console.log('* Server is on http://localhost:4000 ...');
});
