var mongoose = require('mongoose');
// var bcrypt = require('bcrypt');
var Schema = mongoose.Schema;


var userSchema = new Schema({
	firstname:String,
	lastname:String,
	username:String,
	gender:String,
	email:String,
	password:String,
	passwordConf:String,
	friendRequest:[{requestor_id:String,firstname:String,lastname:String,avatar:String}],
	avatar:String,
	friends:[{friend_ID:String,firstname:String,lastname:String,avatar:String}],
	picPath:[{profile:String}],
	postPath:[{
		picture:String,
		description:String,
		comments:[{firstname:String,lastname:String,avatar:String,comment:String}]
	}],
	chatLogs:[{
		name:String,
		message:String
	}]
});

// userSchema.methods.generateHash = function(password){
// 	return bcrypt.hashSync(password,bcrypt.genSaltSync(9));
// }

// userSchema.methods.validPassword = function(password){
// 	return bcrypt.compareSync(password,this.local.password);
// }

console.log('* userSchema ACTIVATED ...');

module.exports = mongoose.model('User',userSchema,'users');
