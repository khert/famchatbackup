var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var newsFeeds = new Schema({
	
	allowed_viewers:[{user_id:String}],
	poster_avatar:String,
	poster_fname:String,
	poster_lname:String,
	poster_like:[{liker_id:String}],
	picture:String,
	description:String,
	comments:[{firstname:String,lastname:String,avatar:String,comment:String}]

	
});



module.exports = mongoose.model('NewsFeeds',newsFeeds,'newfeeds');