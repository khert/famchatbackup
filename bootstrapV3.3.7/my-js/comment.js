
var es2 = new EventSource('/updateComments');


function comment(id){
	var box = document.getElementById('comment-div-'+id);

	var htmlString = 		'<br><div class="input-group" >'+
                                '<input type="text" class="form-control" id="comText-'+id+'" placeholder="Type here ..." style="z-index:0;">'+
                                '<span class="input-group-btn" style="z-index:0;">'+
                                  '<button class="btn btn-success" type="button" onclick="insertComment(\''+id+'\')" >Comment</button>'+
                                '</span>'+
                              '</div>';
                         
    box.insertAdjacentHTML('afterend',htmlString);
    document.getElementById('combut-'+id).onclick = '';

}


function insertComment(id){
	var text = document.getElementById('comText-'+id);
	
	saveCommentToDb(id,text.value);
	
	text.value = '';
}

function postComment(content,id,fname,lname,avatar){

	var box = document.getElementById('comment-box-'+id);
	var comDiv = document.getElementById('comment-div-'+id);

	var htmlString =      '<li>'+
                            '<p><a href=""><img src="'+avatar+'"  height="40" width="40" class="img-circle" align="left">&nbsp&nbsp'+fname+' '+lname+'</a>'+
                            '<p>&nbsp&nbsp'+content+'</p>'+
                            '</p>'+
                          '</li><br>';
    box.insertAdjacentHTML('afterbegin',htmlString);
    comDiv.scrollTop = 0;

}


function saveCommentToDb(id,comment){
	var xhttp2 = new XMLHttpRequest();
	xhttp2.onreadystatechange = function() {
	    
	    if (this.readyState == 4 && this.status == 200) {
	            
	        if(this.responseText == 'ERROR SAVING THE COMMENT'){

	       		toastr.info('COMMENT ERROR','ERROR');

	       	}

	    }

	};
	xhttp2.open("GET", "/saveComment?id="+id+"&comment="+comment, true);
	xhttp2.send();
}


es2.addEventListener('comment', function (event) {
	
	var data = JSON.parse(event.data);

	var box = document.getElementById('comment-box-'+data.poster_ID);

	if(box != undefined || box != null){
		postComment(data.comment,data.poster_ID,data.firstname,data.lastname,data.avatar);
		displayNotificationForComment({firstname:data.firstname,avatar:data.avatar,comment:data.comment});

	}else{
		console.log('THERE IS NO COMMENT BOX ');
	}

});


function displayNotificationForComment(data){



	if(retMyData.firstname != data.firstname){

		var badge = document.getElementById('notification-badge');
		var list = document.getElementById('notification-list');
		var cnt = 1;

		if(badge.style.visibility == 'visible'){

			cnt = parseInt(badge.innerHTML);
			cnt ++;

		}

		badge.innerHTML = cnt;
		badge.style.visibility  = 'visible';

		var htmlString = '<li><a href="/profile?firstname='+data.firstname+'"><img src="'+data.avatar+'" alt="DP" class="img-circle" height="25" width="25" id="own-dp" > '+data.firstname+' like your post</a></li>';

		list.insertAdjacentHTML('afterbegin',htmlString);



		toastr.info(data.firstname+' commented on a post<br> '+data.comment,'Notification');
	
	}
}