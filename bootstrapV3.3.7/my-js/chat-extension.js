
var socket = io();

    var xhttp3 = new XMLHttpRequest();
    xhttp3.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          var data2 = JSON.parse(this.responseText);
          if(data2.name != null || data2.name != ''){
            socket.emit('join',{name:data2.name});
          }
          
        }
      };
      xhttp3.open("GET", "/name", true);
      xhttp3.send();


    socket.on("new_msg", function(data) {
      console.log(data.name);
      console.log(data.message);
        friendReply(data.message,data.name,data.lname);
    });

 function appearMessage(e, id, idCon,to){
       
        if(e.key === "Enter"){
        
          var message = document.getElementById(id);
          var newMessage = message.value.replace(/^\s+|\s+$/g, '');
          message.value = '';
          var messageCon = document.getElementById(idCon);
          var htmlString =   '<div class="arrow-right" style="clear: both;">'+newMessage+'</div>';

          messageCon.insertAdjacentHTML('beforeend',htmlString);
          messageCon.scrollTop = messageCon.scrollHeight - messageCon.clientHeight;


          sendMessage(to,newMessage);
        }
}

function friendReply(mess,fname,lname){

   var messageCon = document.getElementById('message-con'+fname);

   if(messageCon == undefined){

      register_popup(fname, fname+' '+lname ,fname);

      messageCon = document.getElementById('message-con'+fname);
      console.log(messageCon);
   }

    var htmlString =   '<div class="arrow-left" style="clear: both;">'+mess+'</div>';

   messageCon.insertAdjacentHTML('beforeend',htmlString);
   messageCon.scrollTop = messageCon.scrollHeight - messageCon.clientHeight;
}

function sendMessage(to,mess){

    
    var xhttp2 = new XMLHttpRequest();
    
    xhttp2.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        
         var data2 = JSON.parse(this.responseText);
          console.log(data2.status);
         
        }
      };
      xhttp2.open("GET", "/sendToClient?sendTo="+to+"&message="+mess, true);
      xhttp2.send();

}

function updateOnline(){

    var xhttp2 = new XMLHttpRequest();
    xhttp2.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
           var data2 = JSON.parse(this.responseText);
           updateChat(data2);
          
        }
      };
      xhttp2.open("GET", "/updateOnline", true);
      xhttp2.send();
}

function updateChat(data){
      var online = [];
      var friends = data.friendList;
      var allOnline = [];

     
     //GET ALL ONLINE AND ALSO SLICE THERE ID AN PUSH IN ALL ONLINE VARIABLE WITH THE key of ol_ID
      data.onlineList.forEach(function(data){
          var str = data.session;
          str = str.slice(str.length-26,str.length-2);
         allOnline.push({ol_ID:str});
      });

      // GET ALL FRIENDS THAT ARE ONLINE AND STORE IN online variable
      allOnline.forEach(function(data){
          for(i=0;i<friends.length;i++){
            if(friends[i].friend_ID == data.ol_ID){
              online.push({ol_ID:data.ol_ID, firstname: friends[i].firstname});
              break;
            }
          }
      });
      
      //SCAN FRIENDS AND COMPARE TO ONLINE LIST, IF ONLINE SET THE A GREEN DOT, ELSE GRAY
      friends.forEach(function(data){
          var bool = false;
          for(i=0;i<online.length;i++){
              if(data.firstname == online[i].firstname){
                bool = true;
                break;
              }
          }
          if(bool){
               var dot = document.getElementById('dot-'+data.firstname);
               dot.style.color = 'green';
          }else{
               var dot = document.getElementById('dot-'+data.firstname);
               dot.style.color = 'gray';
          }
      });
    
}

setInterval(updateOnline,1000);