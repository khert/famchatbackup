
var retMyData = null;

toastr.options = {
        "debug": false,
        "positionClass": "toast-bottom-left",
        "onclick": null,
        "fadeIn": 300,
        "fadeOut": 1000,
        "timeOut": 5000,
        "extendedTimeOut": 1000
      }

myOwnData();


function setupLikes(){
	var xhttp2 = new XMLHttpRequest();
    xhttp2.onreadystatechange = function() {
	      if (this.readyState == 4 && this.status == 200) {
	    
	             var post = JSON.parse(this.responseText);
	             //myOwnData();
	             setUpLiking(post);
	             	
	             
	       
	     }
	};
	xhttp2.open("GET", "/postData", true);
	xhttp2.send();

}


function myOwnData(){

	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
    
             var post = JSON.parse(this.responseText);
           	 retMyData = post;
           	 setupLikes();

        }
    };
	xhttp.open("GET", "/myData", true);
	xhttp.send();

}



function setUpLiking(data){
	//console.log(data);
	data.forEach(function(item){	

		//console.log(item.poster_like)

		if(item.poster_like.length != 0){

			item.poster_like.forEach(function(rel){
				
				if(rel.liker_id == retMyData._id){

					document.getElementById('like-'+item.poster_ID).style.color = '#5f7ec1';
				}
			});


		}
		
	});
}


function like(id){

	var thumb = document.getElementById('like-'+id);

	if(thumb.style.color == ''){

		youLike(id);

		thumb.style.color = '#5f7ec1';

		if(thumb.innerText == null || thumb.innerText == ''){

			thumb.insertAdjacentHTML('beforeend','1');
		}else{

			var num = parseInt(thumb.innerText);
			num++;

			var htmlString = '<i class="fa fa-thumbs-up icon"></i>';

			thumb.innerText = '';

			htmlString += num;

			thumb.insertAdjacentHTML('beforeend',htmlString);

		}

		
	}else{

		thumb.style.color = '';

		unLike(id);

		if(thumb.innerText != null || thumb.innerText != ''){
			
			var num = parseInt(thumb.innerText);
			num--;

			var htmlString = '<i class="fa fa-thumbs-up icon"></i>';

			thumb.innerText = '';

			if(num > 0 ){

				htmlString += num;

			}

			thumb.insertAdjacentHTML('beforeend',htmlString);
		}
	}
}


function youLike(id){

	var xhttp2 = new XMLHttpRequest();
    xhttp2.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
    		
    		//console.log(this.responseText);
           
        }
    };
    xhttp2.open("GET", "/like?id="+id, true);
    xhttp2.send();

}

function unLike(id){

	var xhttp2 = new XMLHttpRequest();
    xhttp2.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
    		
    		//console.log(this.responseText);
           
        }
    };
    xhttp2.open("GET", "/unLike?id="+id, true);
    xhttp2.send();


}


var esNotif = new EventSource('/updateNotification');

esNotif.addEventListener('notification', function (event) {

	 var data = JSON.parse(event.data);

	 if(data.firstname != retMyData.firstname){

	 	// POST NOTIFICATION
	 	displayNotification(data);
	 }

});


function displayNotification(data){

	

		var badge = document.getElementById('notification-badge');
		var list = document.getElementById('notification-list');
		var cnt = 1;

		if(badge.style.visibility == 'visible'){

			cnt = parseInt(badge.innerHTML);
			cnt ++;

		}

		badge.innerHTML = cnt;
		badge.style.visibility  = 'visible';

		var htmlString = '<li><a href="/profile?firstname='+data.firstname+'"><img src="'+data.avatar+'" alt="DP" class="img-circle" height="25" width="25" id="own-dp" > '+data.firstname+' like your post</a></li>';

		list.insertAdjacentHTML('afterbegin',htmlString);



		toastr.info(data.firstname+' like your post','Notification');
	
	
}


function resetNotif(){

	document.getElementById('notification-badge').style.visibility = 'hidden';
}