

var es = new EventSource('/updateMe');

var profile = '';











var val = setInterval(function(){
            if(profile != '' || profile != 'ERROR'){
                clear();
            }
            myData();
          },1000);



function clear(){
    clearInterval(val);
}


function myData(){
    var xhttp2 = new XMLHttpRequest();
    xhttp2.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
    
             var data2 = JSON.parse(this.responseText);
             profile = data2;

        }
    };
    xhttp2.open("GET", "/myData", true);
    xhttp2.send();
}

function addPhotoFrame(){
    var picon = document.getElementById('pic-container');
    if(picon == undefined){
        
        addPicture();
    }else{

        removePicture();
        addPicture();
    }
    

}
function addPicture(){

    var con =  document.getElementById('post_footer');
    var htmlString =  '<div class="row" style="padding-left: 20px;" id="pic-container">'+
                    '<br><img src="" id="test" height="50" width="100" style="float: left;">'+
                      '<div class="top-right" onclick="removePicture()">'+
                        '<span class="glyphicon glyphicon-remove" id="close"></span>'+
                      '</div>'+
                  '</div>';
    con.insertAdjacentHTML('afterbegin',htmlString);

}

function removePicture(){
    var picon = document.getElementById('pic-container');

    if(picon != undefined || picon != null){

        picon.remove();
    }
}



function readURL(input){
   

    if (input.files && input.files[0]) {
            addPhotoFrame();
            var reader = new FileReader();
            var img = document.getElementById('test');
             
                
            reader.onload = function (e) {
             
                img.src = e.target.result;
            
            }
            reader.readAsDataURL(input.files[0]);
    }

}

function saveMyPost(){

    var content = document.getElementById('post-content');
    var postBut = document.getElementById('button-post');
    var picture = document.getElementById('photo');

    var form = new FormData();

    postBut.disabled = true;


    if(picture.files.length == 0 || picture == undefined){

        var picture = 'no pic';

    }else{

        form.append('photo',picture.files[0]);
    }

    
    var xhttp2 = new XMLHttpRequest();


    xhttp2.onreadystatechange = function () {
        
        if (this.readyState == 4 && this.status == 200) {

            if(this.responseText == 'success'){
                
                toastr.info('Post success','Alert');
                postBut.disabled = false;
                removePicture();
            }else{

                toastr.error('Post Failure','ERROR');
            }
                    
                  
        }
    };

    if(picture == 'no pic'){

        xhttp2.open("GET", "/post?content="+content.value+"&pic="+picture, true);
        xhttp2.send();

    }else{

        xhttp2.open("POST", "/uploadPostPhoto?content="+content.value, true);
        xhttp2.send(form);

    }

    
    content.value = '';

}


es.addEventListener('update', function (event) {
  
    var data = JSON.parse(event.data);

    console.log(data.poster_like);

    if(profile.firstname == data.poster_fname){
      
        displayPost(data._id,data.poster_avatar,data.poster_fname,data.poster_lname,data.picture,data.description,data.comments,data.poster_like);

    }else{

        data.allowed_viewers.forEach(function(item){
            
            if(profile._id == item.user_id){

                displayPost(data._id,data.poster_avatar,data.poster_fname,data.poster_lname,data.picture,data.description,data.comments,data.poster_like);

            }
        });   
    }
});




function displayPost(post_id,avatar,fname,lname,picture,content,comments,likes){

    console.log(likes);

    var allPost = document.getElementById('all-post');

     var htmlString =   '<br>'+
        '<div class="row">'+
            '<div class="panel panel-white post panel-shadow">'+
                '<div class="post-heading">'+
                    '<div class="pull-left image">'+
                        '<img src="'+avatar+'" class="img-circle avatar" alt="user profile image">'+
                    '</div>'+
                    '<div class="pull-left meta">'+
                        '<div class="title h5">'+
                            '<a href="/profile?firstname='+fname+'"><b>'+fname+' '+lname+'</b></a>'+
                            'made a post.'+
                        '</div>'+
                        '<h6 class="text-muted time">a minute ago</h6>'+
                    '</div>'+
                '</div>'+ 
                '<div class="post-description"> ';
                    if(picture == 'no pic'){

                        htmlString += '<p><h2>'+content+'</h2> </p>';

                    }else{

                        htmlString +=   '<p>'+content+'</p>';
                        htmlString +=  '<img src="'+picture+'" height="200" width="300" class="posted-pic">';

                    }      
                var continuation = '<hr>'+
                    '<div class="stats">'+
                        '<a onclick="like(\''+post_id+'\')" class="btn btn-default stat-item" id="like-'+post_id+'">';



                            if(likes == undefined || likes == null){
                                continuation += '<i class="fa fa-thumbs-up icon"></i>';
                            }else{
                                if(likes.length == 0){
                                    continuation += '<i class="fa fa-thumbs-up icon"></i>';
                                }else{
                                    continuation += '<i class="fa fa-thumbs-up icon"></i>'+likes.length;
                                }
                            }

                         
                       continuation += '</a>'+
                        '<a onclick="comment(\''+post_id+'\')" id="combut-'+post_id+'" class="btn btn-default stat-item">'+
                            '<span class="glyphicon glyphicon-comment " ></span> Comment'+
                        '</a>'+
                    '</div>'+
                    '<br>'+
                    '<hr>'+
                      '<div style="max-height: 300px;overflow-y: scroll;background-color: #f7f7f7;" id="comment-div-'+post_id+'">'+
                        '<br>'+
                        '<ul style="list-style: none;" id="comment-box-'+post_id+'">';

                        htmlString+= continuation;
                        var comm = '';

                          for(var i=0;i<comments.length;i++){
                            comm += '<li>'+
                                '<p><a href=""><img src="'+comments[i].avatar+'"  height="40" width="40" class="img-circle" align="left">&nbsp&nbsp'+comments[i].firstname+' '+comments[i].lastname+'</a>'+
                                '<p>&nbsp&nbsp'+comments[i].comment+'</p>'+
                                '</p>'+
                              '</li>'+
                              '<br>';

                          }
                          htmlString += comm;
                        '</ul>'+                             
                      '</div>'+
                '</div>'+
            '</div>'+
        '</div>';

    allPost.insertAdjacentHTML('afterbegin',htmlString);
    
}