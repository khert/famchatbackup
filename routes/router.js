var express = require('express');
var router = express.Router();
var User = require('../models/userSchema');
var Session = require('../models/sessionSchema');
var newsFeed = require('../models/newsFeedsSchema');
var SSE = require('express-sse');
var sse = new SSE(["array", "containing", "initial", "content", "(optional)"]);
var sse2 = new SSE(["array", "containing", "initial", "content", "(optional)"]);
var sse3 = new SSE(["array", "containing", "initial", "content", "(optional)"]);
var sse4 = new SSE(["array", "containing", "initial", "content", "(optional)"]);
var multer = require('multer');
var path = require('path');



var storage = multer.diskStorage({
		destination : './uploads',
		filename : function(req,file,next){
			 next(null,file.fieldname + '-' +Date.now()+path.extname(file.originalname));
		}
});
var upload = multer({
	storage: storage
}).single('photo');



router.get('/',function(req,res){

	console.log(req.io);
	 res.render('indexV3',{status:'good',message:'good'});
});

// WHEN THE USER ATTEMPTS TO LOG IN
// TESTED
router.post('/login',function(req,res){

	var query = User.findOne({username:req.body.username,password:req.body.password});
	query.exec(function(err,user){
		if(err){
			res.send('ERROR !!');
		}else{
			if(user === null){
				res.render('indexV3',{status:'Error!',message:'Invalid username or password.'});
			}else{
				req.session.userId = user._id;
				//req.session.username = user.firstname;
				res.redirect('./home');
			}
		}
	});
});

// WHEN THE USER REGISTERS IN THE WEB
//TESTED
router.post('/register',function(req,res){

	var newUser = new User();
	newUser.username = req.body.username;
	newUser.email = req.body.email;
	newUser.gender = req.body.gender;
	newUser.firstname = req.body.firstname;
	newUser.lastname = req.body.lastname;
	newUser.password = req.body.password;
	// newUser.password = newUser.generateHash(req.body.password);
	//newUser.passwordConf = req.body.password;
	newUser.save(function(err,user){
		if(err){
			res.send('ERROR!!');
		}else{
			req.session.userId = user._id;
			
			res.render('setProfileV3',{firstname:user.firstname,lastname:user.lastname,avatar:'bootstrapV3.3.7/pictures/'+req.body.gender+'.png'});
		}
	});
});


router.get('/test',function(req,res){
	res.send({name:'asdfasdfsafsa213rwe23rare'});
});


router.post('/test',function(req,res){
	console.log('SUCCESS');
	res.send('SUCCESSS');
});


router.get('/addFriend',function(req,res){
	//console.log(req.query.fname);
	//console.log(req.session.userId);
	//res.send({name:'SUCCESSFULLY ADDED!!!'});
	User.findById(req.session.userId)
	.exec(function(err,user){
		User.findOneAndUpdate({firstname:req.query.fname},{$push:{ friendRequest:{"requestor_id":user._id,"firstname":user.firstname,"lastname":user.lastname,"avatar":user.avatar}}},{upsert:true},function(err,list){
			if(err){
				res.send('ERROR !!');
			}else{
				//console.log(list);
				res.send('success');
			}
		});
	});
	

			
	// res.sendFile(__dirname+'/sample hover.html');
});



router.get('/profile',function(req,res){

	//var query = User.findOne({firstname:req.query.firstname});
	//var query = User.find();
	User.find(function(err,search){



	//query.exec(function(err,search){
		if(err){
			res.send('ERROR !!');
		}else{
			var mySearch = '';
			search.forEach(function(item){
				if(item.firstname == req.query.firstname){
					mySearch = item;
				}
			});
			if(mySearch == ''){
				res.send('USER NOT FOUND!! NAME');
			}
			// if(search === null){
			// 	res.send('USER NOT FOUND!! NAME');
			// }
			else{
				User.findById(req.session.userId).exec(function(err,user){
					if(err){
						res.send('ERROR!!')
					}else{
						if(!user){
							res.render('error');
						}else{
							newsFeed.find(function(err,list){
								if(err){
									console.log(err);
								}else{

									var userPost = getAllMyPost(req.query.firstname,list);

									//console.log(userPost);

									if(user.firstname === mySearch.firstname){
										res.render('profileV3Own',{
										avatar:user.avatar,
										firstname:user.firstname,
										lastname:user.lastname,
										postDatas:userPost,
										friends:user.friends

									});

									}else{
										var friends = user.friends;
										var request = user.friendRequest;
										var result = {"result":""};
										friends.forEach(function(item){
											if(item.firstname === mySearch.firstname){
												result.result = 'friends';
											}	
										});
										request.forEach(function(item2){
											if(item2.firstname === mySearch.firstname){
												result.result = 'request';
											}	
										});
										if(result.result === ""){
											result.result = 'add';
										}


										res.render('profileV3',{
										avatar:user.avatar,
										firstname:user.firstname,
										lastname:user.lastname,
										avatarS:mySearch.avatar,
										firstnameS:mySearch.firstname,
										lastnameS:mySearch.lastname,
										status: result.result,
										postDatas:userPost,
										friends:mySearch.friends

									 });

									}	

								}
							});		
							
						}		
					}
				});
			}
		}
	});

});


function getAllMyPost(name,allPost){
	var userPost = [];


			for(var i=0;i<allPost.length;i++){

				if(allPost[i].poster_fname == name){
					userPost.push({
							poster_ID:allPost[i]._id,
							poster_avatar:allPost[i].poster_avatar,
							poster_fname:allPost[i].poster_fname,
							poster_lname:allPost[i].poster_lname,
							picture:allPost[i].picture,
							description:allPost[i].description,
							poster_like : getLikes(allPost[i].poster_like),
							comments: getComments(allPost[i].comments)
					});
					
				}

			}
	
	return userPost;
}


router.get('/update',function(req,res){
	User.findById(req.session.userId).exec(function(err,user){
		if(err){
			res.send('ERROR!!')
		}else{
			res.json(user.friendRequest);
		}
	});

});

router.get('/accept',function(req,res){
	console.log(req.query.id);

	User.findById(req.query.id).exec(function(err,user){
		if(err){
			res.send('ERROR!! finding the user who added you..')
		}else{

			User.findByIdAndUpdate(req.session.userId,{$push:{ friends:{"friend_ID":user._id,"firstname":user.firstname,"lastname":user.lastname,"avatar":user.avatar}}},{upsert:true},function(err,list){
				if(err){
					res.send('ERROR!! adding the user who added you.');
				}else{
					

					User.findByIdAndUpdate(req.query.id,{$push:{ friends:{ "friend_ID":list._id,"firstname":list.firstname,"lastname":list.lastname,"avatar":list.avatar}}},{upsert:true},function(err,list2){
						if(err){
							res.send('ERROR!! adding your data as a friend from the user who added you..');
						}else{

							User.findByIdAndUpdate(req.session.userId,{$pull:{ friendRequest:{"requestor_id":req.query.id} }},{new:true},function(err,list3){
								if(err){
									res.send('ERROR!! deleting the request of the user you want to accept as a friend.')
								}else{
									res.send({status:'success',message:list3.firstname+' is successfully added.',friends:list3.friends});
								}
							});
						}
					});
				}		
			});
		}
	});
});

router.get('/myData',function(req,res){
	User.findById(req.session.userId,function(err,data){
		if(err){
			res.send('ERROR');
		}else{
			res.send(data);
		}
	});
});

router.get('/addChatCon',function(req,res){
	console.log(req.query.id + ' ****************************************************');
});



router.get('/updateOnline',function(req,res){
	var all = {};
	Session.find(function(err,list){		
		if(err){
			res.send('ERROR');
		}else{
			all.onlineList = list;
			User.findById(req.session.userId)
			.exec(function(err,user){
				if(err){
					res.send('ERROR!!')
				}else{
					all.friendList = user.friends;
					res.json(all);
				}
			});	      		
		}
	});	
});


// router.get('/updateMe',sse, function(req,res){
// 	res.sse()
// });

router.get('/updateMe',sse.init);


router.get('/post',function(req,res){
	
	var newFeed = new newsFeed();

	User.findById(req.session.userId,function(err,data){
		if(err){
			res.send('ERROR');
		}else{
			var cnt = 0;
			data.friends.forEach(function(user){
				newFeed.allowed_viewers[cnt] = {'user_id':user.friend_ID};
				cnt++;
			});
			newFeed.poster_avatar = data.avatar;
			newFeed.poster_fname = data.firstname;
			newFeed.poster_lname = data.lastname;
			newFeed.picture = req.query.pic;
			newFeed.description = req.query.content;
			
			newFeed.save(function(err,feed){
				if(err){
					res.send('ERROR SAVING THE POST');
					console.log('ERROR SAVING POST ...');
				}else{

					sendPostData(feed);
					res.send('success');
					console.log('SUCCESS SAVING POST ...');
				}
			});	
		}
	});
});


function sendPostData(feed){
	sse.send(feed,'update');
}



router.get('/name',function(req,res){

	res.send({name:req.session.userId});
					
});


router.get('/home',function(req,res){

	User.findById(req.session.userId)
	.exec(function(err,user){
		if(err){
			res.send('ERROR!!')
		}else{
			if(!user){
				res.render('error');
			}else{
				var post = [];

				newsFeed.find(function(err,data){
					if(err){
						res.send('ERROR!!')
					}else{
						var myData = getRelatedPost(data,user._id,user.firstname);
						//console.log(data[0].allowed_viewers);
						//console.log(myData);
						var id = user._id;
						res.render('homeV3',{
							avatar:user.avatar,
							firstname:user.firstname,
							lastname:user.lastname,
							friends:user.friends,
							postDatas:myData,
							cur_userID:id
						});
				     }

				});
			}		
		}
	});
});

router.get('/postData',function(req,res){
	User.findById(req.session.userId)
	.exec(function(err,user){
		if(err){
			res.send('ERROR!!')
		}else{
			if(!user){
				res.render('error');
			}else{
				var post = [];

				newsFeed.find(function(err,data){
					if(err){
						res.send('ERROR!!')
					}else{
						var myData = getRelatedPost(data,user._id,user.firstname);
						//console.log(data[0].allowed_viewers);
						console.log('POST ALL USER DATA SUCCESS');
						res.send(myData);
				     }

				});
			}		
		}
	});

});

function getRelatedPost(data,userID,fnameOfRequestor){
	var related = [];

	data.forEach(function(post){
		
		if(fnameOfRequestor == post.poster_fname){
			related.push({
					poster_ID:post._id,
					poster_avatar:post.poster_avatar,
					poster_fname:post.poster_fname,
					poster_lname:post.poster_lname,
					picture:post.picture,
					description:post.description,
					poster_like : getLikes(post.poster_like),
					comments: getComments(post.comments)
			});
		}		
		post.allowed_viewers.forEach(function(id){
			//console.log(id)
			if(userID == id.user_id){
				related.push({
					poster_ID:post._id,
					poster_avatar:post.poster_avatar,
					poster_fname:post.poster_fname,
					poster_lname:post.poster_lname,
					picture:post.picture,
					description:post.description,
					poster_like : getLikes(post.poster_like),
					comments: getComments(post.comments)
				});
			}
		});
	});
	//console.log(data);
	//related[0].user_id;
	return related;
}
function getComments(data){
	var comments = [];

	data.forEach(function(com){
		comments.push({firstname:com.firstname,lastname:com.lastname,avatar:com.avatar,comment:com.comment});
	});
	return comments;
}

function getLikes(data){
	var likes = [];

	data.forEach(function(com){
		likes.push({liker_id: com.liker_id})
	});
	// console.log('***********************')
	// console.log(likes);
	// console.log('***********************')
	return likes;
}


router.get('/updateComments',sse2.init);

router.get('/saveComment',function(req,res){
	User.findById(req.session.userId).exec(function(err,user){
		if(err){
			res.send('ERROR LOOKING FOR THE COMMENTOR');
			console.log('ERROR LOOKING FOR THE COMMENTOR ...');
		}else{
			//res.send('SUCCESS LOOKING FOR THE COMMENTOR');
			//console.log('SUCCESS SAVING COMMENT ...');
			//console.log(user);
			newsFeed.findByIdAndUpdate(req.query.id,{$push:{ comments:{"firstname":user.firstname,"lastname":user.lastname,"avatar":user.avatar,"comment":req.query.comment}}},{upsert:true},function(err,post){
				if(err){
					res.send('ERROR SAVING THE COMMENT');
					console.log('ERROR SAVING COMMENT ...');
				}else{

					sendComments({"poster_ID":post._id,"firstname":user.firstname,"lastname":user.lastname,"avatar":user.avatar,"comment":req.query.comment});
					res.send('SUCCESS SAVING THE COMMENT');
					console.log('SUCCESS SAVING COMMENT ...');
					//console.log(post);
				}
			});
		}

	});
	
});


function sendComments(comment){
	sse2.send(comment,'comment');
}




router.post('/uploadPostPhoto',function(req,res){
	
	upload(req,res,function(err){
		if(err){
			console.log('ERROR UPLOADING THE POSTED PHOTO !!');
		}else{
			console.log('SUCCESS SAVING THE POSTED PHOTO');
			res.redirect('./savePostWithPhoto?content='+req.query.content+'&picture=uploads/'+req.file.filename);
		}
	});
});

router.get('/savePostWithPhoto',function(req,res){

	var newFeed = new newsFeed();

	User.findById(req.session.userId,function(err,data){
		if(err){
			console.log('ERROR LOOKING FOR USER WHO UPLOAD A POST WITH PHOTO');
		}else{
			var cnt = 0;
			data.friends.forEach(function(user){
				newFeed.allowed_viewers[cnt] = {'user_id':user.friend_ID};
				cnt++;
			});
			newFeed.poster_avatar = data.avatar;
			newFeed.poster_fname = data.firstname;
			newFeed.poster_lname = data.lastname;
			newFeed.picture = req.query.picture;
			newFeed.description = req.query.content;
			
			newFeed.save(function(err,feed){
				if(err){
					res.send('ERROR SAVING THE POST');
					console.log('ERROR SAVING POST ...');
				}else{
					sendPostData(feed);
					res.send('success');
					console.log('SUCCESS SAVING POST WITH PIC ...');
				}
			});	
		}
	});
});





var file ='';



router.post('/upload',(req,res)=>{

	upload(req,res,(err)=>{
	     if(err){
			console.log('Theres an error');
		}else{
			file = req.file;
			res.redirect('./save');
		}	
	});

});




router.get('/save',function(req,res){
		User.findByIdAndUpdate(req.session.userId,{$set:{ "avatar":"uploads/"+file.filename}},{upsert:true},function(err,list){
				if(err){
					console.log('ERROR SAVING IMAGES');
				}else{
					 res.redirect('./home');
				}
		});
});



router.get('/like',function(req,res){
	console.log(req.query.id);
	newsFeed.findByIdAndUpdate(req.query.id, {$push:{ poster_like:{liker_id:req.session.userId}}},{upsert:true},function(err,feed){
		if(err){
			console.log(err);
			res.send(err);
		}else{
			User.findById(req.session.userId,function(err,data){
				if(err){
					console.log('ERROR LOOKING FOR USER WHO UPLOAD A POST WITH PHOTO');
				}else{
					console.log('SUCCESS LIKE');
					res.redirect('./notification?firstname='+data.firstname+'&avatar='+data.avatar+'&posterfname='+feed.poster_fname);
					
				}
			});
		}	
	});

});

router.get('/unLike',function(req,res){
	console.log(req.query.id);
	newsFeed.findById(req.query.id,function(err,feed){
		if(err){
			console.log(err);
			res.send(err);
		}else{
			
			for(var i=0;i<feed.poster_like.length;i++){
				if(feed.poster_like[i].liker_id == req.session.userId){
					// console.log('PARIHA SILA')
					feed.poster_like[i].remove();
					// console.log(feed);
				}
			}
			feed.save(function(err,newFeed){
				if(err){
					console.log(err);
					res.send(err);
				}else{
					// console.log(newFeed);
					res.send('SUCCESS UNLIKE');
					console.log('SUCCESS UNLIKE');
				}
			});

		}	
	});

});



router.get('/notification',function(req,res){
	//console.log(req.query.firstname+ ' like your post');
	User.findById(req.session.userId,function(err,user){
		if(err){
			console.log(err);
			res.send(err);
		}else{
			
			console.log(user.firstname +" !=  "+ req.query.posterfname);
			if(user.firstname != req.query.posterfname){
				sendNotification({firstname:req.query.firstname,avatar:req.query.avatar});
			}
			res.send(req.query.liker+' like your post');

		}
	});
	


});

router.get('/updateNotification',sse3.init);

function sendNotification(data){
	sse3.send(data,'notification');
}



router.get('/chat',function(req,res){
	

});





router.get('/logout',function(req,res){
	    if(req.session){
			req.session.destroy(function(err){
				if(err){
					res.send('ERROR'+' ');
				}else{
					res.redirect('/');
				}
			});
		}
});






console.log('* Router Activated ...');
console.log('* SSE Activated ...');

module.exports = {'route':router,'sse':sse,'multer':multer};